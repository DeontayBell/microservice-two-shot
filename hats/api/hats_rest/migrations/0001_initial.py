# Generated by Django 4.0.3 on 2023-06-01 17:20

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hats',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo', models.CharField(max_length=100)),
                ('colors', models.CharField(max_length=20)),
                ('size', models.IntegerField(blank=True, null=True)),
            ],
        ),
    ]
