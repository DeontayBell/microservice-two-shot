from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})



class Hats(models.Model):
    name = models.CharField(max_length=100, blank=True)
    logo = models.CharField(max_length=100)
    colors = models.CharField(max_length=20)
    size = models.IntegerField(blank=True, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name
