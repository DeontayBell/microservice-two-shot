
from .models import Hats
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json



class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "id",
        "logo",
        "colors",
        "size",
    ]




@require_http_methods(["GET", "POST"])
def api_list_hat(request):
    if request.method == "GET":
        hat = Hats.objects.all()
        return JsonResponse(
            {"hat": hat},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):

    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 405
            return response
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            hat = Hats.objects.get(id=pk)

            props = ["name", "logo", "id"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatsListEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
