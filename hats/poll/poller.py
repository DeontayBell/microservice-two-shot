import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            response = requests.get('wardrobe-api:8000/api/locations')
            wardrobe_api = response.json()
            for location in wardrobe_api:
                location_vo = LocationVO.objects.create(
                    closet_name=location['closet_name'],
                    section_number=location['section_number'],
                    shelf_number=location['shelf_number']
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
