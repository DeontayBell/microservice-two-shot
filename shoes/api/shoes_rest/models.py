from django.db import models

# Create your models here.


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    name= models.CharField(max_length=100)
    color= models.CharField(max_length=100)
    picture= models.URLField(max_length=500)

    def __str__(self):
        return self.name
