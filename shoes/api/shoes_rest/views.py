from django.shortcuts import render
from .models import Shoes
from common.json import ModelEncoder
from django.http import JsonResponse
# Create your views here.


class ShoeEncoder(ModelEncoder):
    model = Shoes
    properties= [
        "id",
        "manufacturer",
        "name",
        "color",
        "picture",

    ]



def api_shoes(request):
    shoe= Shoes.objects.all()
    return JsonResponse(
        shoe,
        encoder = ShoeEncoder,
        safe = False
    )
