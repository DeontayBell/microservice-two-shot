function App() {
  const hatInfo = fetch('http://localhost:8090/api/hats/')
    .then(response => response.json())
    .catch(error => console.error(error))
    console.log(hatInfo);

  return (
    <div>
      <h1>Hats List</h1>
      <table>
        <thead>
          <tr>
            <th>Unfortunately wasn't able to figure out how to display the data😭</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{hatInfo.data}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
